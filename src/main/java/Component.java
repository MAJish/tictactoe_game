import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class Component extends JComponent {
    private final int empty=0;
    private final int x=100;
    private final int o =1000;
    private Integer [][] game;
    private Boolean turn=true;
    /**
     * Метод для вызова отрисовки поля и крестиков ноликов
     * @param graf
     */
    public void paint(Graphics graf){
        super.paintComponent(graf);
        graf.clearRect(0,0,getWidth(),getHeight());
        pole(graf);
        drewiXO(graf);
    }

    /**
     * Метод для отрисовки крестика и нолика
     * @param graf
     */
    private void drewiXO(Graphics graf){
        for (int i=0;i<3;++i){
            for (int j=0;j<3;++j){
                if (game[i][j]==this.x){
                    drawX(i,j,graf);
                }
                else if(game[i][j]==this.o){
                    draw0(i,j,graf);
                }
            }
        }
    }

    /**
     * Описание отрисовки крестика
     * @param i столбец для отрисовки
     * @param j строка для отрисовки
     * @param graf
     */
    private void drawX (int i,int j,Graphics graf) {
        graf.setColor(Color.RED);
        Integer width = getWidth()/3;
        Integer height = getHeight()/3;
        graf.drawLine(width*i,height*j,width*i+width,height*j+height);
        graf.drawLine(width*i,height*j+height,width*i+width,height*j);
        }

    /**
     * Описание отрисовки нолика
     * @param i столбец для отрисовки
     * @param j строка для отрисовки
     * @param graf
     */
    private void draw0(int i,int j,Graphics graf){
        graf.setColor(Color.BLUE);
        Integer width = getWidth()/3;
        Integer height = getHeight()/3;
        graf.drawOval(width*i+4*width/100,height*j,width*9/10,height);
    }
    /**
     * Метод для отрисовки поля 3 x 3
     * @param graf
     */
    private void pole (Graphics graf) {
        graf.setColor(Color.BLACK);
        Integer width = getWidth();
        Integer height = getHeight();
        for (int i =1;i<3;i++){
            graf.drawLine(0,(height/3)*i, width ,(height/3)*i);
            graf.drawLine((width/3)*i,0, (width/3)*i ,height);
        }
    }

    /**
     * Конструктор класса. Включаем возможность получение событий от мыши и задаем поле 3 на 3.
     */
    public Component(){
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
        this.game=new Integer[3][3];
        clears();
    }

    /**
     * Заполняет массив нулями
     */
    private void clears(){
        for (int i=0;i<3;++i){
            for(int j=0;j<3; ++j){
                this.game[i][j]=this.empty;
            }
        }

    }

    /**
     * Метод для отслеживаем в какой клетке был сделан клик, и какой символ был поставлен
     * @param event Нажатая кнопка
     */
    protected void processMouseEvent(MouseEvent event) {
        if (event.getButton() == MouseEvent.BUTTON1) {
            int x = event.getX();
            int y = event.getY();
            int i = (int) ((float) x / getWidth() * 3);
            int j = (int) ((float) y / getHeight() * 3);

            if (this.game[i][j] == empty) {
                if (turn) {
                    this.game[i][j] = this.x;
                    turn = false;
                } else {
                    this.game[i][j] = this.o;
                    turn = true;
                }
                repaint();
                int win = winners();

                if (win != 0) {
                    if (win == this.o * 3) {
                        JOptionPane.showMessageDialog(this, "O won!", "You win!", JOptionPane.INFORMATION_MESSAGE);
                    }
                    if (win == this.x * 3) {
                        JOptionPane.showMessageDialog(this, "X won!", "You win", JOptionPane.INFORMATION_MESSAGE);

                    } else if (win != this.o * 3) {
                        JOptionPane.showMessageDialog(this, "Draw!", "Draw =(", JOptionPane.INFORMATION_MESSAGE);
                    }
                    clears();
                    repaint();

                }
            }
        }
    }

    /**
     * Метод для определения победителя
     * @return запоминает число, с помощью которого определяем победителя
     */
    private Integer winners(){
        int d=0;
        int d1=0;
        for (int i = 0; i<3;i++){
            d+=this.game[i][i];
            d1+=this.game[i][2-i];
        }
        if(d==this.o * 3 || d == this.x * 3){
            return d;
        }
        if(d1==this.o * 3 || d1 == this.x * 3){
            return d1;
        }
        int check1,chek2;
        boolean empty=false;
        for(int i=0;i<3;i++) {
            check1=0;
            chek2=0;
            for(int j=0;j<3;j++){
                if(this.game[i][j]==0){
                    empty=true;
                }
                check1+=this.game[i][j];
                chek2+=this.game[j][i];
            }
            if(check1==this.x*3 || check1==this.o*3){

                return check1;
            }
            if(chek2==this.x*3 || chek2==this.o*3){

                return chek2;
            }
        }
        if(empty){
            return 0;
        }
        else
            return -1;
    }

}
